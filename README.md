# Public Theme repository

To add a theme, just create a new directory in the form themename-author
Add your zipped theme file(s) and add an entry to the main array in theme.json file as
described below:

```json
// The following theme is a demonstration and is ignored by Recalbox
{
  /* Let's start with the "properties" object that collect 
     important informations about the theme.
     All field are mandatory */
  "properties":
  {
    /* Then you must enter a few information regarding your theme
       Let's start with the folder where all data are stored
       the form name-author must be privilegied */
    "folder": "demo-recalbox",
    // Then the user-friendly name and the author
    "name": "demo",
    "author": "bkg2k",
    // And a short description
    "description": "This is not a true theme and must be ignored by theme processors.\nThis is only intended to demonstrate all available properties.",
    // Then the current version (must match the version in the <theme version="..."> attribute)
    "version": "1.0",
    // and finally the minimum recalbox version required to render your theme properly
    "fromrecalbox": "9.2"
  },
  /* Then the illustration bloc, required to show what your theme render
     Only the "hdmi" sub object is mandatory. Other tate/crt blocks are used
     to render appropriate images to the user whis *is* in this mode, when
     the theme support them. */
  "illustrations":
  {
    /* Each individual block can contain an arbitrary number of screenshots.
       Please use screenshot in the appropriate resolution. In Hdmi, use the highest possible resolution
       supported by your theme. In crt mode, use 320x240 screenshots.  */
    "hdmi":
    {
      // SystemList screenshots list
      "systemlist": [ "system1.png", "system2.png", "system3.png", "system4.png" ],
      // GameList screenshots list - When possible, use systems not already in the SystelList
      "gamelist": [ "game1.png", "game2.png", "game3.png", "game4.png" ],
      // Menu & Windows screenshots. The start menu and BIOS checker are good choices for example
      "menu": [ "menu1.png", "menu2.png" ]
    },
    /* All the three following objects are "overriden" images and are optional.
       This means, if they are defined, they will replace corresponding lists of hdmi block
       when the user's recalbox is actually in the same mode.
       For exemple, if your theme support TATE modes, it's way better to show TATE images
       when the user's frontend is also in TATE mode */
    "hdmitate":
    {
      "systemlist": [ "systemtate1.png", "systemtate2.png", "systemtate3.png" ],
      "gamelist": [ "gametate1.png", "gametate2.png", "gametate3.png", "game4.png" ],
      "menu": [ "menutate1.png", "menutate2.png" ]
    },
    "crt":
    {
      "systemlist": [ "systemcrt1.png", "systemcrt2.png", "systemcrt3.png", "systemcrt4.png" ],
      "gamelist": [ "gamecrt1.png", "gamecrt2.png", "gamecrt3.png" ],
      "menu": [ "menucrt1.png", "menucrt2.png" ]
    },
    "crttate":
    {
      "systemlist": [ "systemcrttate1.png", "systemcrttate2.png", "systemcrttate3.png", "systemcrttate4.png" ],
      "gamelist": [ "gamecrttate1.png", "gamecrttate2.png", "gamecrttate3.png", "gamecrttate4.png" ],
      "menu": [ "menucrttate1.png" ]
    }
  },
  /* Finally the data block will provide theme folder along with supported compatibility & resolutions
     For most theme, only one object is required.
     For heavy and multi-resolution themes, like the famous "Next-Pixels", you may déclare as many data object
     you need. The first object matching display compatibility & resolution of the user will be actually
     downloaded and installed. */
  "data":
  [
    {
      "folder": "demo1",
      "compatiblity": "hdmi",
      "resolutions": "vga,hd,fhd"
    },  
    {
      "folder": "demo2",
      "compatiblity": "crt,jamma",
      "resolutions": "qvga,vga"
    }
  ]
},
```